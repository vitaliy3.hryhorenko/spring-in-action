import com.springinaction.knights.BraveKnight;
import com.springinaction.knights.Quest;
import com.springinaction.knights.QuestException;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class BraveKnightTest {

    @Test
    public void knightShouldEmbarkOnQuest() throws QuestException {
        Quest mockQuest = mock(Quest.class);  // Создание фиктивного
        // объекта Quest
        BraveKnight knight = new BraveKnight(mockQuest);
        // Внедрение
        knight.embarkOnQuest();
        verify(mockQuest, times(1)).embark();
    }
}